package com.example.adrianmatuszewski.news_world.Model.response;

/**
 * @author: Adrian Matuszewski
 */
public class SuccessFailResponse {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
