package com.example.adrianmatuszewski.news_world.Presenter;



import com.example.adrianmatuszewski.news_world.Model.DataSource;
import com.example.adrianmatuszewski.news_world.Model.GenericCallback;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;
import com.example.adrianmatuszewski.news_world.Model.response.NewsResponse;
import com.example.adrianmatuszewski.news_world.Model.RetrofitDataSource;
import com.example.adrianmatuszewski.news_world.Model.response.VoteResponse;
import com.example.adrianmatuszewski.news_world.View.OpenFullNewsView;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * @author: Adrian Matuszewski
 */
public class OpenFullNewsPresenter<T extends OpenFullNewsView> extends Presenter<T>  {

    private int mId;

    private DataSource mDataSource;
    private News mNews;

    public OpenFullNewsPresenter(final Context context) {
        mDataSource = new RetrofitDataSource(context);
    }



    public void prepareChosenNews(int id) {
        mId = id;
        mDataSource.getChosenNews(id, new GenericCallback<NewsResponse>() {
            @Override
            public void onSuccess(final NewsResponse newsResponse) {
                mNews = newsResponse.getNews();
                mView.setChosenNews(mNews);

            }

            @Override
            public void onFailure(final Exception error) {
                Toast.makeText(mView.getContext(), "New account created", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void voteNews(final boolean userVote) {
        Log.d("Vote", "voteNews");
        if (mNews.isVoted() == null) {
            mDataSource.postVote(mId, userVote, new GenericCallback<VoteResponse>() {
                @Override
                public void onSuccess(VoteResponse data) {
                    updateProgressBar();
                    Log.d("Vote", "postVote onSuccess");
                    showMessageBar(userVote);
                }

                @Override
                public void onFailure(Exception error) {
                    Log.d("Vote", "postVote onFailure");
                    mView.showProblemSnackBar("Post vote");
                }
            });
        } else {
            if(mNews.isVoted() == userVote) {
                mView.showProblemSnackBar("It is a same vote");
            } else {
                mDataSource.putVote(mId, userVote, new GenericCallback<VoteResponse>() {
                    @Override
                    public void onSuccess(VoteResponse data) {
                        updateProgressBar();
                        Log.d("Vote", "putVote onSuccess");
                        showMessageBar(userVote);
                    }

                    @Override
                    public void onFailure(Exception error) {
                        Log.d("Vote", "putVote onFailure");
                        mView.showProblemSnackBar("Put vote");
                    }
                });
            }
        }
    }
    private void updateProgressBar() {
        mDataSource.getChosenNews(mId, new GenericCallback<NewsResponse>() {
            @Override
            public void onSuccess(final NewsResponse newsResponse) {
                mNews = newsResponse.getNews();
                mView.setProgressBar(mNews.getVoices());
                mView.setVoiceDownTextView(mNews.getDownVotes());
                mView.setVoiceUpTextView(mNews.getUpVotes());
            }

            @Override
            public void onFailure(final Exception error) {
                Toast.makeText(mView.getContext(), "New account created", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showMessageBar(boolean userVote) {
        if (userVote == true) {
            mView.showLikeSnackBar();
        } else {
            mView.showDislikeSnackBar();
        }

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void finish() {

    }

}
