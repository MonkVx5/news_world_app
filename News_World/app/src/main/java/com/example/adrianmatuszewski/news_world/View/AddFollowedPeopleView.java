package com.example.adrianmatuszewski.news_world.View;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.FollowedPerson;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;

import android.content.Context;

import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public interface AddFollowedPeopleView extends BaseView {

    Context getContext();

    void setList(List<FollowedPerson> list);


}
