package com.example.adrianmatuszewski.news_world.Model.request;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Vote;

/**
 * @author: Adrian Matuszewski
 */
public class VoteRequest {

    private Vote vote;

    public VoteRequest(Vote vote) {
        this.vote = vote;
    }
}
