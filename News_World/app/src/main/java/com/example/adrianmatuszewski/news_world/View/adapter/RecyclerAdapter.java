package com.example.adrianmatuszewski.news_world.View.adapter;


import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;
import com.example.adrianmatuszewski.news_world.R;
import com.example.adrianmatuszewski.news_world.View.helper.ImageDisplayHelper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


/**
 * Adapter for list for news
 * author: Adrian Matuszewski
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.NewsViewHolder> {

    private final int mItemLayout;
    private final Context mContext;
    private List<News> mAdapterListOfNews;
    private ImageDisplayHelper mImageDisplayHelper;

    public RecyclerAdapter(final Context context, final List<News> news, final int itemLayout) {
        mContext = context;
        mAdapterListOfNews = news;
        mItemLayout = itemLayout;
        mImageDisplayHelper = new ImageDisplayHelper();
    }
    

    @Override
    public NewsViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NewsViewHolder holder, final int position) {
        final News news = mAdapterListOfNews.get(position);
        holder.mTittle.setText(String.format(news.getTittle()));
       // holder.mCategory.setText(String.format(news.getCategory().getName()));
        holder.mDate.setText(String.format(news.getPublishDate()));
        mImageDisplayHelper.displayBitmapUrl(mContext, news.getPhotoUrl(), holder.mPhoto);
    }
    
    
    public void animateTo(final List<News> news) {
        applyAndAnimateRemovals(news);
        applyAndAnimateAdditions(news);
        applyAndAnimateMovedItems(news);
        
    }
    
    private void applyAndAnimateRemovals(final List<News> news) {
        for (int i = mAdapterListOfNews.size() - 1; i >= 0; i--) {
            final News currentNews = mAdapterListOfNews.get(i);
            if(!news.contains(currentNews)) {
                removeItem(i);
            }
        }
    }
    
    private void applyAndAnimateAdditions(final List<News> listOfNewNews) {
        for (int i = 0, b = listOfNewNews.size(); i < b; i++) { // todo remove b
            final News newNews = listOfNewNews.get(i);
            if(!mAdapterListOfNews.contains(newNews)) { // +1 :)
                addItem(i, newNews);
            }
        }
    }
    
    private void applyAndAnimateMovedItems(final List<News> news) {
        for (int toPosition = news.size() - 1; toPosition >= 0; toPosition--) {
            final News currentNews = news.get(toPosition);
            final int fromPosition = mAdapterListOfNews.indexOf(currentNews);
            if(fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
                
            }
        }
    }
    
    public News removeItem(final int position) {
        final News model = mAdapterListOfNews.remove(position);
        notifyItemRemoved(position);
        return model;
    }
    
    public void addItem(final int position, final News model) {
        mAdapterListOfNews.add(position, model);
        notifyItemInserted(position);
    }
    
    public void moveItem(final int fromPosition, final int toPosition) {
        final News model = mAdapterListOfNews.remove(fromPosition);
        mAdapterListOfNews.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public int getItemCount() {
        return mAdapterListOfNews.size();
    }

    static class NewsViewHolder extends RecyclerView.ViewHolder {
        final public TextView mTittle;
        final public TextView mCategory;
        final public TextView mDate;
        final public ImageView mPhoto;

        public NewsViewHolder(final View itemView) {
            super(itemView);
            mTittle = (TextView) itemView.findViewById(R.id.news_tittle);
            mCategory =(TextView) itemView.findViewById(R.id.news_category);
            mDate = (TextView) itemView.findViewById(R.id.news_date);
            mPhoto = (ImageView) itemView.findViewById(R.id.news_photo);
        }

    }

}
