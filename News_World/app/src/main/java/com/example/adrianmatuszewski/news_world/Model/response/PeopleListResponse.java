package com.example.adrianmatuszewski.news_world.Model.response;


import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.FollowedPerson;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;

import java.util.List;
/**
 * @author: Adrian Matuszewski
 */
public class PeopleListResponse {

    private List<FollowedPerson> followed_people;

    public List<FollowedPerson> getListOfPeople() {
            return followed_people;
    }

    public void setListOfPeople(List<FollowedPerson> followed_people) {
        this.followed_people = followed_people;
    }

}