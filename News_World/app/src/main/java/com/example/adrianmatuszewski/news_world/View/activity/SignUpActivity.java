package com.example.adrianmatuszewski.news_world.View.activity;

import com.example.adrianmatuszewski.news_world.Presenter.SignUpPresenter;
import com.example.adrianmatuszewski.news_world.R;

import com.example.adrianmatuszewski.news_world.View.SignUpView;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * author: Adrian Matuszewski
 */
public class SignUpActivity extends AppCompatActivity implements SignUpView {

    private SignUpPresenter mSignUpPresenter;
    @Bind(R.id.user_email)
    EditText mUserEmail;
    @Bind(R.id.user_name)
    EditText mUserName;
    @Bind(R.id.user_password)
    EditText mPassword;
    @Bind(R.id.user_password_confirmation)
    EditText mPasswordConfirmation;
    @Bind(R.id.sign_up__button)
    Button mSignUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        mSignUpPresenter = new SignUpPresenter(this);
        mSignUpPresenter.setView(this);
    }

    @OnClick(R.id.sign_up__button)
    public void handleSignUpButton() {
        final String email = mUserEmail.getText().toString();
        mUserEmail.setText("");
        final String username = mUserName.getText().toString();
        mUserName.setText("");
        final String password = mPassword.getText().toString();
        mPassword.setText("");
        final String passwordConfirmation = mPasswordConfirmation.getText().toString();
        mPasswordConfirmation.setText("");
        mSignUpPresenter.tryCreateAccount(email, username, password, passwordConfirmation);
    }

    @Override
    public void goToLogInActivity() {
        finish();
    }

    @Override
    public void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mUserName.getWindowToken(), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSignUpPresenter.resume();
    }

    @Override
    public void finish() {
        super.finish();
    }

}
