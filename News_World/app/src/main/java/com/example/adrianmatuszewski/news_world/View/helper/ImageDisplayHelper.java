package com.example.adrianmatuszewski.news_world.View.helper;


import com.bumptech.glide.Glide;
import com.example.adrianmatuszewski.news_world.R;

import android.content.Context;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.widget.ImageView;


/**
 * This class is responsible for displaying pictures and resizing photos taken. Use Picasso library.
 *
 * @author Adrian Matuszewski
 *
 */

public class ImageDisplayHelper {

    public void displayBitmapUrl(final Context context, final String url, final ImageView imageView) {
            if (imageView.getWidth() == 0) {
                imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        Glide.with(context).load(url).centerCrop().placeholder(R.drawable.map_euope).override(imageView.getHeight(), imageView.getHeight())
                                .into(imageView);
                    }
                });
            } else {
                Glide.with(context).load(url).centerCrop().placeholder(R.drawable.map_euope).override(imageView.getWidth(), imageView.getHeight())
                        .into(imageView);
            }
    }
}
