package com.example.adrianmatuszewski.news_world.View.activity;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.FollowedPerson;
import com.example.adrianmatuszewski.news_world.Presenter.AddFollowedPeoplePresenter;
import com.example.adrianmatuszewski.news_world.Presenter.ListOfNewsPresenter;
import com.example.adrianmatuszewski.news_world.Presenter.UserSettingsPresenter;
import com.example.adrianmatuszewski.news_world.R;
import com.example.adrianmatuszewski.news_world.View.AddFollowedPeopleView;
import com.example.adrianmatuszewski.news_world.View.adapter.CategoryRecyclerAdapter;
import com.example.adrianmatuszewski.news_world.View.adapter.PeopleRecyclerAdapter;
import com.example.adrianmatuszewski.news_world.View.adapter.RecyclerViewItemClickListener;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author: Adrian Matuszewski
 */
public class AddFollowedPeopleActivity extends AppCompatActivity implements AddFollowedPeopleView {

    @Bind(R.id.list_of_people)
    RecyclerView mRecyclerView;
    @Bind(R.id.name)
    EditText mNameEditText;
    @Bind(R.id.surname)
    EditText mSurnameEditText;
    @Bind(R.id.profession)
    EditText mProfessionEditText;
    private List<FollowedPerson> mList;

    private AddFollowedPeoplePresenter mAddFollowedPeoplePresenter;
    private PeopleRecyclerAdapter mPeopleRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follwed_people);
        ButterKnife.bind(this);
        mAddFollowedPeoplePresenter = new AddFollowedPeoplePresenter(this);
        mAddFollowedPeoplePresenter.setView(this);
        mAddFollowedPeoplePresenter.prepareListOfFollowedPeople();
        hideSoftKeyboard();

    }

    @OnClick(R.id.add_followed_person_button)
    public void addPerson() {
        mAddFollowedPeoplePresenter.addFollowedPerson(mNameEditText.getText().toString(), mSurnameEditText.getText().toString(),
                mProfessionEditText.getText().toString());
        mAddFollowedPeoplePresenter.prepareListOfFollowedPeople();
        mNameEditText.setText("");
        mSurnameEditText.setText("");
        mProfessionEditText.setText("");
    }
    
    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setList(final List<FollowedPerson> list) {
        mList = new ArrayList<>();
        for (FollowedPerson a : list) {
            mList.add(a);
        }

        mPeopleRecyclerAdapter = new PeopleRecyclerAdapter(this, mList,  R.layout.category_option_item);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(mPeopleRecyclerAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(this, new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public boolean onItemClick(final View view, final int position) {
                if(mPeopleRecyclerAdapter.isButtonClicked()) {
                    mAddFollowedPeoplePresenter.deletePerson(mList.get(position).getId());
                    mAddFollowedPeoplePresenter.prepareListOfFollowedPeople();
                }
                return true;
            }
        }));

    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
