package com.example.adrianmatuszewski.news_world.Model.dataBaseModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author: Adrian Matuszewski
 */
public class Vote implements Serializable {

    @SerializedName(VoteConstant.VALUE)
    private boolean mValue;

    public Vote(boolean value) {
        mValue = value;
    }

    public boolean isValue() {
        return mValue;
    }

    public void setValue(boolean value) {
        mValue = value;
    }

    public interface VoteConstant {
        String VALUE = "value";
    }
}
