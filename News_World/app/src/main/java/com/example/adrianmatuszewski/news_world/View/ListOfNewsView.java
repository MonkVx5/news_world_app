package com.example.adrianmatuszewski.news_world.View;


import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;

import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.SearchView;

import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public interface ListOfNewsView extends BaseView, SearchView.OnQueryTextListener {

    Context getContext();

    void setList(List<News> list);

    void prepareCategoryTableLayout(List<Category> list);

    void goToClickedNews(final int chosenNews);

    void changeCurrentList(final List<News> list);

    void initNavigationDrawer();

    void openSettings();

    void signOut();

    boolean isNavDrawerOpen();

    void closeNavDrawer();

    void openAddFollowedPeople();

}
