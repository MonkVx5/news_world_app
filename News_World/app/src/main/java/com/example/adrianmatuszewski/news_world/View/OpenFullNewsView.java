package com.example.adrianmatuszewski.news_world.View;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;

import android.content.Context;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author: Adrian Matuszewski
 */
public interface OpenFullNewsView extends BaseView  {

    Context getContext();

    void setChosenNews(final News news);

    void backToList();

    void likeNews();

    void dislikeNews();

    void showLikeSnackBar();

    void showDislikeSnackBar();

    void showProblemSnackBar(final String message);

    void setVoiceUpTextView(final int voicesUp);

    void setVoiceDownTextView(final int voicesDown);

    void setProgressBar(final double voices);
}
