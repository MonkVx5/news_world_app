package com.example.adrianmatuszewski.news_world.Model.request;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.User;

/**
 * @author: Adrian Matuszewski
 */
public class UserRequest {

    private User user;

    public UserRequest(User user) {
        this.setUser(user);
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
