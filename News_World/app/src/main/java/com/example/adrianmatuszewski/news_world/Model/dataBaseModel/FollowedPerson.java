package com.example.adrianmatuszewski.news_world.Model.dataBaseModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author: Adrian Matuszewski
 */
public class FollowedPerson implements Serializable {

    @SerializedName(FollowedPersonConstant.KEY_ID)
    private int mId;
    @SerializedName(FollowedPersonConstant.NAME)
    private String mName;
    @SerializedName(FollowedPersonConstant.SURNAME)
    private String mSurname;
    @SerializedName(FollowedPersonConstant.PROFESSION)
    private Profession mProfession;

    public FollowedPerson(String name, String surname, String professionName) {
        setName(name);
        setSurname(surname);
        setProfession(new Profession(professionName));
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public String getProfessionName() {
        return mProfession.getProfessionName();
    }

    public void setProfession(Profession profession) {
        mProfession = profession;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }


    private class Profession implements Serializable{

        @SerializedName(FollowedPersonConstant.PROFESSION_NAME)
        private String mProfessionName;

        public Profession (String professionName) {
            mProfessionName = professionName;
        }

        public String getProfessionName() {
            return mProfessionName;
        }
    }

    public interface FollowedPersonConstant {
        String KEY_ID = "id";
        String NAME = "first_name";
        String SURNAME = "second_name";
        String PROFESSION = "profession";
        String PROFESSION_NAME = "name";
    }
}
