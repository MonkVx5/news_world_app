package com.example.adrianmatuszewski.news_world.Model;


import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.FollowedPerson;
import com.example.adrianmatuszewski.news_world.Model.response.CategoryListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.NewsListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.NewsResponse;
import com.example.adrianmatuszewski.news_world.Model.response.PeopleListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SignOutResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SignUpResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SuccessFailResponse;
import com.example.adrianmatuszewski.news_world.Model.response.UserResponse;
import com.example.adrianmatuszewski.news_world.Model.response.VoteResponse;

import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public interface DataSource {

    void getAllNews(GenericCallback<NewsListResponse> callbackResponse);

    void logIn(String username, String password, GenericCallback<UserResponse> userGenericCallback);

    void readNewNews(String lastDate, GenericCallback<NewsListResponse> callbackResponse);

    void signUp(String email, String username, String password, String passwordConfirmation, GenericCallback<SignUpResponse> userGenericCallback );

    void getChosenNews(int id, GenericCallback<NewsResponse> userGenericCallback);

    void putVote(int id, boolean voteValue, GenericCallback<VoteResponse> voteResponseGenericCallback);

    void postVote(int id,boolean voteValue, GenericCallback<VoteResponse> voteResponseGenericCallback);

    void signOut(GenericCallback<SignOutResponse> userGenericCallback);

    void getCategories(GenericCallback<CategoryListResponse> categoryListResponseGenericCallback);

    void getUserMe(GenericCallback<UserResponse> userMeResponseGenericCallback);

    void getUserNews(GenericCallback<NewsListResponse> userNewsResponseCallback);

    void getTopNews(GenericCallback<NewsListResponse> userNewsResponseCallback);

    void setUser(List<Integer> categoriesIds, GenericCallback<UserResponse> setUserResponseCallback);

    void postFollowedPeople(FollowedPerson followedPerson,  GenericCallback<SuccessFailResponse> userGenericCallback );
    void getFollowedPeople( GenericCallback<PeopleListResponse> userGenericCallback);
    void deleteFollowedPeople(final int id, GenericCallback<SuccessFailResponse> userGenericCallback);
    void getFollowedPeopleNews(GenericCallback<NewsListResponse> userNewsResponseCallback);
    void putFollowedPeople(FollowedPerson followedPerson, GenericCallback<SuccessFailResponse> userGenericCallback); //moze refaktor ?



}
