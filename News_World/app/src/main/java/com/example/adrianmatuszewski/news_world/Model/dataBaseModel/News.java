package com.example.adrianmatuszewski.news_world.Model.dataBaseModel;

import com.google.gson.annotations.SerializedName;

import android.util.Log;

import java.io.Serializable;

/**
 * @author: Adrian Matuszewski
 */
public class News implements Serializable{

    @SerializedName(NewsConstants.KEY_ID)
    private int mId;
    @SerializedName(NewsConstants.KEY_TITTLE)
    private String mTittle;
    @SerializedName(NewsConstants.KEY_SOURCE)
    private String mSource;
    @SerializedName(NewsConstants.KEY_DESCRIPTION)
    private String mDescription;
    @SerializedName(NewsConstants.KEY_CONTENT)
    private String mContent;
    @SerializedName(NewsConstants.KEY_PHOTO_URL)
    private String mPhotoUrl;
    @SerializedName(NewsConstants.KEY_DATE)
    private String mPublishDate;
    @SerializedName(NewsConstants.KEY_CATEGORY)
    private Category mCategory;
    @SerializedName(NewsConstants.KEY_UP_VOTES)
    private int mUpVotes;
    @SerializedName(NewsConstants.KEY_DOWN_VOTES)
    private int mDownVotes;
    @SerializedName(NewsConstants.KEY_VOTED)
    private Boolean isVoted;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getTittle() {
        return mTittle;
    }

    public void setTittle(String name) {
        mTittle = name;
    }

    public String getSource() {
        return mSource;
    }

    public void setSource(String source) {
        mSource = source;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPublishDate() {
        return mPublishDate;
    }

    public void setPublishDate(String publishDate) {
        mPublishDate = publishDate;
    }

    public Category getCategory() {
        return mCategory;
    }

    public void setCategory(Category category) {
        mCategory = category;
    }

    public int getUpVotes() {
        return mUpVotes;
    }

    public void setUpVotes(int upVotes) {
        mUpVotes = upVotes;
    }

    public int getDownVotes() {
        return mDownVotes;
    }

    public void setDownVotes(int downVotes) {
        mDownVotes = downVotes;
    }

    public Boolean isVoted() {
        return isVoted;
    }

    public void setVoted(boolean voted) {
        isVoted = voted;
    }

    public double getVoices() {
        double voices = 0;
        if (mUpVotes > 0 ) {
            voices =  ( (double)mUpVotes / ((double) mUpVotes + (double) mDownVotes)) * 100;
        }
        Log.d("Voices:","mUpVotes ="+ mUpVotes + "| mDownVotes=" + mDownVotes + "| VOICES = " + voices );
        return voices;
    }


    public interface NewsConstants {
        String KEY_ID = "id";
        String KEY_TITTLE = "title";
        String KEY_SOURCE = "source";
        String KEY_DATE = "publish_date";
        String KEY_CONTENT = "text";
        String KEY_PHOTO_URL = "image_url";
        String KEY_DESCRIPTION = "description";
        String KEY_CATEGORY = "category";
        String KEY_UP_VOTES = "up_votes";
        String KEY_DOWN_VOTES = "down_votes";
        String KEY_VOTED = "voted";
    }
    
}
