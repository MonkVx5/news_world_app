package com.example.adrianmatuszewski.news_world.Presenter;

import com.example.adrianmatuszewski.news_world.Model.DataSource;
import com.example.adrianmatuszewski.news_world.Model.GenericCallback;
import com.example.adrianmatuszewski.news_world.Model.RetrofitDataSource;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.example.adrianmatuszewski.news_world.Model.response.CategoryListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.UserResponse;
import com.example.adrianmatuszewski.news_world.View.UserSettingsView;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public class UserSettingsPresenter <T extends UserSettingsView> extends Presenter<T> {


    private DataSource mDataSource;
    private ArrayList<Category> mListOfCategories;

    public UserSettingsPresenter(final Context context) {
        mDataSource = new RetrofitDataSource(context);
        prepareCategories();
    }


    public void prepareCategories() {
        mDataSource.getCategories(new GenericCallback<CategoryListResponse>() {
            @Override
            public void onSuccess(final CategoryListResponse data) {
                mListOfCategories = (ArrayList<Category>) data.getCategories();
                prepareUserCurrentSettings();
            }

            @Override
            public void onFailure(final Exception error) {
                error.getMessage();
            }
        });
    }

    //TODO: W sumie backendie każda klategoria powinna miec swój boolean
    public void prepareUserCurrentSettings() {
        mDataSource.getUserMe(new GenericCallback<UserResponse>() {
            @Override
            public void onSuccess(final UserResponse data) {
                ArrayList<Category> userCategories = (ArrayList<Category>) data.getUser().getListOfCategory();
                mListOfCategories.removeAll(userCategories);
                for (Category category : userCategories) {
                    mListOfCategories.get(category.getId() - 1).setIsSetByUser(true);
                }

                if (mListOfCategories != null) {
                    mView.setListOfCategories(mListOfCategories);
                }

            }

            @Override
            public void onFailure(final Exception error) {
                error.getMessage();
            }
        });
    }

    public void setUserSetting (final List<Integer> categoryIds) {
        mDataSource.setUser(categoryIds, new GenericCallback<UserResponse>() {
            @Override
            public void onSuccess(final UserResponse data) {
                Log.d("setUserSettings", "sukces");
            }

            @Override
            public void onFailure(final Exception error) {
                Log.d("setUserSettings", "porazka");
            }
        });

    }
    
    @Override
    void resume() {

    }

    @Override
    void pause() {

    }

    @Override
    void destroy() {

    }

    @Override
    public void finish() {

    }
}
