package com.example.adrianmatuszewski.news_world.Model.response;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;

/**
 * @author: Adrian Matuszewski
 */
public class NewsResponse {

    private News news;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }
}
