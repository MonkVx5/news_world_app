package com.example.adrianmatuszewski.news_world.View.activity;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.example.adrianmatuszewski.news_world.Presenter.UserSettingsPresenter;
import com.example.adrianmatuszewski.news_world.R;
import com.example.adrianmatuszewski.news_world.View.UserSettingsView;
import com.example.adrianmatuszewski.news_world.View.adapter.CategoryRecyclerAdapter;
import com.example.adrianmatuszewski.news_world.View.adapter.RecyclerViewItemClickListener;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author: Adrian Matuszewski
 */
public class UserSettingsActivity extends AppCompatActivity implements UserSettingsView {

    @Bind(R.id.list_of_categories)
    RecyclerView mRecyclerView;

    private UserSettingsPresenter mUserSettingsPresenter;
    private CategoryRecyclerAdapter mCategoryRecyclerAdapter;
    private List<Category> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setting);
        ButterKnife.bind(this);

        mUserSettingsPresenter = new UserSettingsPresenter(this);
        mUserSettingsPresenter.setView(this);

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setListOfCategories(final List<Category> list) {
        mList = new ArrayList<>();
        for (Category a : list) {
            mList.add(a);
        }

        mCategoryRecyclerAdapter = new CategoryRecyclerAdapter(this, mList,  R.layout.category_option_item);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(mCategoryRecyclerAdapter);
        mRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(this, new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public boolean onItemClick(final View view, final int position) {
                Log.d("categorie", " : " + position);
                return true;
            }
        }));

    }

    @OnClick(R.id.save_btn)
    public void setSaveButton() {
        mUserSettingsPresenter.setUserSetting(mCategoryRecyclerAdapter.getSwitchStatus());
        finish();
    }
}
