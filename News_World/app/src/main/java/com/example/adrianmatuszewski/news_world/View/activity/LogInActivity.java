package com.example.adrianmatuszewski.news_world.View.activity;

import com.example.adrianmatuszewski.news_world.Presenter.LogInPresenter;
import com.example.adrianmatuszewski.news_world.R;
import com.example.adrianmatuszewski.news_world.View.LogInView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * author: Adrian Matuszewski
 */
public class LogInActivity extends AppCompatActivity implements LogInView {

    private LogInPresenter mLogInPresenter;
    @Bind(R.id.user_name)
    EditText mUserName;
    @Bind(R.id.user_password)
    EditText mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        mLogInPresenter = new LogInPresenter(this);
        mLogInPresenter.setView(this);
    }

    @OnClick(R.id.log_in_button)
    public void handleLogInButton() {
        final String username = mUserName.getText().toString();
        mUserName.setText("");
        final String password = mPassword.getText().toString();
        mPassword.setText("");
        mLogInPresenter.tryLogIn(username, password);
    }

    @OnClick(R.id.sign_up__button)
    public void handleSignUpButton() {
        mLogInPresenter.goToSignUpActivity();
    }

    @Override
    public void goToListOfNewsActivity() {
        Intent intent = new Intent(this, ListOfNewsActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToSignActivity() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    @Override
    public void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mUserName.getWindowToken(), 0);
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(this, UserSettingsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLogInPresenter.resume();
    }

    @Override
    public void finish() {
        super.finish();
    }
}
