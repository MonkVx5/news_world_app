package com.example.adrianmatuszewski.news_world.View.activity;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;
import com.example.adrianmatuszewski.news_world.Presenter.OpenFullNewsPresenter;
import com.example.adrianmatuszewski.news_world.R;
import com.example.adrianmatuszewski.news_world.View.OpenFullNewsView;
import com.example.adrianmatuszewski.news_world.View.helper.ImageDisplayHelper;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * author: Adrian Matuszewski
 */
public class OpenFullNewsActivity extends AppCompatActivity implements OpenFullNewsView {

    public static final String NEWS_EXTRA = "param.news";

    private OpenFullNewsPresenter mOpenFullNewsPresenter;
    private ImageDisplayHelper mImageDisplayHelper;

    @Bind(R.id.news_tittle)
    public TextView mTittle;
    @Bind(R.id.news_category)
    public TextView mSource;
    @Bind(R.id.news_date)
    public TextView mDate;
    @Bind(R.id.news_content)
    public TextView mContent;
    @Bind(R.id.up_voice_number)
    public TextView mVoiceUpTextView;
    @Bind(R.id.down_voice_number)
    public TextView mVoiceDownTextView;
    @Bind(R.id.progress_bar)
    public ProgressBar mProgressBar;
    @Bind(R.id.taken_photo_image_view)
    public ImageView mPhoto;
    @Bind(R.id.coordinator_layout_full_news)
    CoordinatorLayout mCoordinatorLayout;


    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_news);
        ButterKnife.bind(this);
        mOpenFullNewsPresenter = new OpenFullNewsPresenter(this);
        mOpenFullNewsPresenter.setView(this);
        mImageDisplayHelper = new ImageDisplayHelper();

        if (getIntent().getExtras() != null) {
            Bundle chosenNews = getIntent().getExtras();
            int newsId = chosenNews.getInt(NEWS_EXTRA);
            mOpenFullNewsPresenter.prepareChosenNews(newsId);
        } else {
            throw new RuntimeException("This activity should always have some extras");
        }

    }
    
    @Override
    public void setChosenNews(final News news) {
        mTittle.setText(news.getTittle());
        mSource.setText(news.getSource());
        mDate.setText(news.getPublishDate());
        mContent.setText(news.getContent());
        mImageDisplayHelper.displayBitmapUrl(this, news.getPhotoUrl(), mPhoto);
        setVoiceUpTextView(news.getUpVotes());
        setVoiceDownTextView(news.getDownVotes());
        setProgressBar(news.getVoices());
   }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void backToList() {
        finish();
    }

    @Override
    @OnClick(R.id.fab_like)
    public void likeNews() {
        Log.d("Vote", "fab_like");
        mOpenFullNewsPresenter.voteNews(true);
    }

    @Override
    @OnClick(R.id.fab_dislike)
    public void dislikeNews() {
        Log.d("Vote", "fab_dislike");
        mOpenFullNewsPresenter.voteNews(false);
    }

    @Override
    public void showLikeSnackBar() {
        Snackbar.make(mCoordinatorLayout, "You like this!", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showDislikeSnackBar() {
        Snackbar.make(mCoordinatorLayout, "You dislike this", Snackbar.LENGTH_SHORT).show();

    }

    @Override
    public void showProblemSnackBar(final String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setVoiceUpTextView(final int voicesUp) {
        mVoiceUpTextView.setText(Integer.toString(voicesUp));
    }

    @Override
    public void setVoiceDownTextView(final int voicesDown) {
        mVoiceDownTextView.setText(Integer.toString(voicesDown));
    }

    @Override
    public void setProgressBar(final double voices) {
        mProgressBar.setProgress((int) voices);
    }

}
