package com.example.adrianmatuszewski.news_world.View.adapter;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.FollowedPerson;
import com.example.adrianmatuszewski.news_world.R;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * @author: Adrian Matuszewski
 */
public class PeopleRecyclerAdapter extends RecyclerView.Adapter<PeopleRecyclerAdapter.PersonViewHolder> {

    private final List<FollowedPerson> mFollowedPersonList;
    private final Context mContext;
    private final int mItemLayout;
    private boolean isButtonPressed = true;


    public PeopleRecyclerAdapter(final Context context, final List<FollowedPerson> categoryList, final int itemLayout) {
        mFollowedPersonList = categoryList;
        mItemLayout = itemLayout;
        mContext = context;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.followed_person_item, parent, false);
        return new PersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder holder, final int position) {
        final FollowedPerson followedPerson = mFollowedPersonList.get(position);
        holder.mNameView.setText(followedPerson.getName());
        holder.mSurnameView.setText(followedPerson.getSurname());
        holder.mProfessionView.setText(followedPerson.getProfessionName());
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mFollowedPersonList.size();
    }



    public boolean isButtonClicked() {
        return isButtonPressed;
    }


    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        private TextView mNameView;
        private TextView mSurnameView;
        private TextView mProfessionView;
        private Button mButton;

        public PersonViewHolder(final View itemView) {
            super(itemView);
            mNameView = (TextView) itemView.findViewById(R.id.name);
            mSurnameView = (TextView) itemView.findViewById(R.id.surname);
            mProfessionView = (TextView) itemView.findViewById(R.id.profession);
            mButton = (Button) itemView.findViewById(R.id.remove_btn);
        }
    }
    
}
