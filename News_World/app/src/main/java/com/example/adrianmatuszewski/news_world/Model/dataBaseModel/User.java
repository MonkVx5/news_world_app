package com.example.adrianmatuszewski.news_world.Model.dataBaseModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public class User implements Serializable {

    @SerializedName(CurrentUserConstants.KEY_USERNAME)
    private String mUsername;
    @SerializedName(CurrentUserConstants.KEY_PASSWORD)
    private String mPassword;
    @SerializedName(CurrentUserConstants.KEY_PASSWORD_CONFIRMATION)
    private String mPasswordConfirmation;
    @SerializedName(CurrentUserConstants.KEY_EMAIL)
    private String mEmail;
    @SerializedName(CurrentUserConstants.KEY_CATEGORIES)
    private boolean mCategoriesSet;
    @SerializedName(CurrentUserConstants.KEY_SESSION)
    private Session mSession;
    @SerializedName(CurrentUserConstants.CATEGORIES)
    private List<Category> mListOfCategory;
    @SerializedName(CurrentUserConstants.CATEGORY_IDS)
    private List<Integer> mCategoryIds;
    @SerializedName(CurrentUserConstants.FOLLOWED_PERSON)
    private FollowedPerson mFollowedPerson;

    public User() {

    }

    public User(FollowedPerson followedPerson) {
        setFollowedPerson(followedPerson);
    }

    public User(String username, String password) {
        setUsername(username);
        setPassword(password);
    }

    public User(String email, String username, String password, String passwordConfirmation) {
        setEmail(email);
        setUsername(username);
        setPassword(password);
        setPasswordConfirmation(passwordConfirmation);
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPasswordConfirmation() {
        return mPasswordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        mPasswordConfirmation = passwordConfirmation;
    }

    public boolean isCategoriesSet() {
        return mCategoriesSet;
    }

    public void setCategoriesSet(boolean categoriesSet) {
        mCategoriesSet = categoriesSet;
    }

    public Session getSession() {
        return mSession;
    }

    public List<Category> getListOfCategory() {
        return mListOfCategory;
    }

    public void setListOfCategory(List<Category> listOfCategory) {
        mListOfCategory = listOfCategory;
    }

    public List<Integer>  getCategoryIds() {
        return mCategoryIds;
    }

    public void setCategoryIds(List<Integer> categoryIds) {
        mCategoryIds = categoryIds;
    }

    public void setSession(Session session) {
        mSession = session;
    }

    public FollowedPerson getFollowedPerson() {
        return mFollowedPerson;
    }

    public void setFollowedPerson(FollowedPerson followedPerson) {
        mFollowedPerson = followedPerson;
    }


    public class Session {
        private String token;

        public Session(String token) {//cfab020e-b5f9-4e11-9034-1b3816ca8754
            this.token = token;
        }

        public String getToken() {
            return token;
        }

    }
    public interface CurrentUserConstants {
        String KEY_USERNAME= "login";
        String KEY_PASSWORD = "password";
        String KEY_PASSWORD_CONFIRMATION = "password_confirmation";
        String KEY_EMAIL = "email";
        String KEY_CATEGORIES = "categories_set";
        String KEY_SESSION = "session";
        String CATEGORIES = "categories";
        String CATEGORY_IDS = "category_ids";
        String FOLLOWED_PERSON = "followed_person";
    }

}
