package com.example.adrianmatuszewski.news_world.View;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;

import android.content.Context;

import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public interface UserSettingsView extends BaseView {

    Context getContext();

    void setListOfCategories(List<Category> list);

}
