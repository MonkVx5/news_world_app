package com.example.adrianmatuszewski.news_world.View;

/**
 * @author: Adrian Matuszewski
 */
public interface LogInView extends BaseView {

    void goToListOfNewsActivity();


    void goToSignActivity();

    void hideKeyboard();

    void openSettings();

    void finish();
    
}
