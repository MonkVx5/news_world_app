package com.example.adrianmatuszewski.news_world.View.adapter;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.example.adrianmatuszewski.news_world.R;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.CategoryViewHolder> {

    private final List<Category> mCategoryList;
    private final Context mContext;
    private final int mItemLayout;


    private List<Integer> mUserCategories;

    public CategoryRecyclerAdapter (final Context context, final List<Category> categoryList, final int itemLayout) {
        mCategoryList = categoryList;
        mItemLayout = itemLayout;
        mContext = context;
        mUserCategories = new ArrayList<>();
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_option_item, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, final int position) {
        final Category category = mCategoryList.get(position);
        holder.mCategoryItem.setText(category.getName());
        holder.mCategoryItem.setChecked(category.isSetByUser());
        holder.mCategoryItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                category.setIsSetByUser(isChecked);
            }
        });
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public List<Integer> getSwitchStatus() {
        for (Category category : mCategoryList) {
            if (category.isSetByUser()) {
                mUserCategories.add(category.getId());
            }
        }
        return mUserCategories;
    }


    public static class CategoryViewHolder extends RecyclerView.ViewHolder {

        public final Switch mCategoryItem;

        public CategoryViewHolder(final View itemView) {
            super(itemView);
            mCategoryItem = (Switch) itemView.findViewById(R.id.category_item);
        }
    }
    
}
