package com.example.adrianmatuszewski.news_world.View.activity;



import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;
import com.example.adrianmatuszewski.news_world.Presenter.ListOfNewsPresenter;
import com.example.adrianmatuszewski.news_world.R;
import com.example.adrianmatuszewski.news_world.View.ListOfNewsView;
import com.example.adrianmatuszewski.news_world.View.adapter.RecyclerAdapter;
import com.example.adrianmatuszewski.news_world.View.adapter.RecyclerViewItemClickListener;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * author: Adrian Matuszewski
 */
public class ListOfNewsActivity extends BaseActivity implements ListOfNewsView {

    private static final String NONE_CATEGORY = "All";

    @Bind(R.id.list_of_news)
    RecyclerView mListOfNews;

    protected ActionBarDrawerToggle mDrawerToggle;
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.navigation_view)
    NavigationView mNavigationView;
    @Bind(R.id.list_of_news_content)
    CoordinatorLayout mCoordinatorLayout;

    @Bind(R.id.tab_layout)
    TabLayout mTabLayout;

    @Nullable
    @Bind(R.id.action_search)
    SearchView mSearchView;

    private ListOfNewsPresenter mListOfNewsPresenter;

    private RecyclerAdapter mRecyclerAdapter;
    private List<News> mList;

    private TabLayout.Tab mTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_news);
        ButterKnife.bind(this);
        setToolbar();
        mListOfNewsPresenter = new ListOfNewsPresenter(this);
        mListOfNewsPresenter.setView(this);
        initNavigationDrawer();
        prepareTableLayoutListener();

    }

    @Override
    public void prepareCategoryTableLayout(final List<Category> categories) {
        mTabLayout.removeAllTabs();
        mTabLayout.addTab(mTabLayout.newTab().setText(NONE_CATEGORY));
        for (Category category : categories) {
            mTabLayout.addTab(mTabLayout.newTab().setText(category.getName()));
        }
    }

    private void prepareTableLayoutListener() {
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(final TabLayout.Tab tab) {
                if (mTab == null || !mTab.equals(tab)) {
                    mTab = tab;
                    Log.d("dlaczego ja", "wio");
                    String categoryName = tab.getText().toString();
                    if (mRecyclerAdapter != null) {
                        filterCategory(categoryName);
                    }
                }
            }

            @Override
            public void onTabUnselected(final TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(final TabLayout.Tab tab) {

            }
        });
    }

    private void filterCategory(final String categoryName) {
        String category = categoryName;
        if (categoryName.equals(NONE_CATEGORY)) {
            category = "";
        } else {
            category = categoryName;
        }
        final List<News> filteredListOfNews = mListOfNewsPresenter.filterByCategory(category.toLowerCase());
        mRecyclerAdapter.animateTo(filteredListOfNews);
        mListOfNews.scrollToPosition(0);

    }


    public void initNavigationDrawer() {
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        setupActionBarDrawerToogle();
        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView);
        }
    }

    private void setupActionBarDrawerToogle() {

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        );
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    private void setupDrawerContent(NavigationView navigationView) {

        //setting up selected item listener
        navigationView.setCheckedItem(R.id.nav_all);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem menuItem) {
                menuItem.setChecked(true);
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.nav_top:
                        Snackbar.make(mNavigationView, "Top News!", Snackbar.LENGTH_SHORT).show();
                        mListOfNewsPresenter.showTopNews();
                        break;

                    case R.id.nav_my:
                        Snackbar.make(mNavigationView, "My News!", Snackbar.LENGTH_SHORT).show();
                        mListOfNewsPresenter.showMyNews();
                        break;

                    case R.id.nav_all:
                        Snackbar.make(mNavigationView, "All News!", Snackbar.LENGTH_SHORT).show();
                        mListOfNewsPresenter.showAllNews();
                        break;

                    case R.id.nav_sign__out:
                        mListOfNewsPresenter.signOut();
                        break;
                    case R.id.nav_followed_people_news:
                        mListOfNewsPresenter.showFollowedPeopleNews();
                        break;

                    case R.id.nav_followed_people:
                        openAddFollowedPeople();
                        break;

                    case R.id.nav_settings:
                        openSettings();
                        break;
                }
                mDrawerLayout.closeDrawers();
                return true;
            }
        });

    }

    @Override
    public Context getContext() {
        return this;
    }

    //TODO: I should to refactor this solution!
    @Override
    public void setList(final List<News> list) {
        mList = new ArrayList<>();
        for (News a : list) {
            mList.add(a);
        }
        mRecyclerAdapter = new RecyclerAdapter(this, mList, R.layout.news_item);
        mListOfNews.setAdapter(mRecyclerAdapter);

        mListOfNews.setHasFixedSize(true);
        mListOfNews.setLayoutManager(new LinearLayoutManager(this));

        mListOfNews.setItemAnimator(new DefaultItemAnimator());
        mListOfNews.addOnItemTouchListener(new RecyclerViewItemClickListener(this, new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public boolean onItemClick(final View view, final int position) {
                goToClickedNews(mList.get(position).getId());
                Log.d("Lista", " : " + position);
                return true;
            }
        }));

    }

    @Override
    public void changeCurrentList(final List<News> list) {
            mRecyclerAdapter.animateTo(list);
            mListOfNews.scrollToPosition(0);
            mSearchView.onActionViewCollapsed();
            mSearchView.clearFocus();
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(this, UserSettingsActivity.class);
        startActivity(intent);
    }

    //TODO: Need to refactor this solution!
    @Override
    public void goToClickedNews(final int chosenNews) {
        Bundle newsToOpen = new Bundle();
        newsToOpen.putSerializable(OpenFullNewsActivity.NEWS_EXTRA, chosenNews);
        Intent intent = new Intent(this, OpenFullNewsActivity.class);
        intent.putExtras(newsToOpen);
        startActivity(intent);
    }

    @Override
    public void signOut() {
        Intent intent = new Intent(this, LogInActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        mSearchView = (SearchView) myActionMenuItem.getActionView();
        mSearchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mListOfNewsPresenter.resume();
    }

    @Override
    protected void onPause() {
        mListOfNewsPresenter.pause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mListOfNewsPresenter.destroy();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.sign_out:
                mListOfNewsPresenter.signOut();
                break;
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(final String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        final List<News> filteredListOfNews = mListOfNewsPresenter.filter(newText.toLowerCase());
        mRecyclerAdapter.animateTo(filteredListOfNews);
        mListOfNews.scrollToPosition(0);
        return true;
    }



    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public boolean isNavDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }

    public void closeNavDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void openAddFollowedPeople() {
        Intent intent = new Intent(this, AddFollowedPeopleActivity.class);
        startActivity(intent);
    }

}
