package com.example.adrianmatuszewski.news_world.Model;



import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.FollowedPerson;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Vote;
import com.example.adrianmatuszewski.news_world.Model.request.UserRequest;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.User;
import com.example.adrianmatuszewski.news_world.Model.request.VoteRequest;
import com.example.adrianmatuszewski.news_world.Model.response.CategoryListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.NewsListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.NewsResponse;
import com.example.adrianmatuszewski.news_world.Model.response.PeopleListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SignOutResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SignUpResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SuccessFailResponse;
import com.example.adrianmatuszewski.news_world.Model.response.UserResponse;
import com.example.adrianmatuszewski.news_world.Model.response.VoteResponse;
import com.example.adrianmatuszewski.news_world.R;

import android.content.Context;
import android.util.Log;

import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;

/**
 * @author: Adrian Matuszewski
 */
public class RetrofitDataSource implements DataSource {

    private static final String TAG_LOG_IN = "LogIn :";
    private static final String TAG_SIGN_IN = "SignIn :";
    private static String mCurrentToken;
    private static User mTmpUser;

    private static RestAdapter mRestAdapter = new RestAdapter.Builder()
            .setEndpoint(ParseConfig.PARSE_ENDPOINT)
            .setLogLevel(RestAdapter.LogLevel.FULL).setLog(new AndroidLog(ParseConfig.RETROFIT_LOG))
            .build();

    private static RequestInterceptor mRequestInterceptor;
    private Context mContext;
    private RequestAPI mAPI;

    public RetrofitDataSource(final Context context) {
        mAPI = mRestAdapter.create(RequestAPI.class);
        mContext = context;
    }

    private static void setHeader(final String token) {
        mCurrentToken = ParseConfig.TOKEN + token;
    }

    public void getAllNews(final GenericCallback<NewsListResponse> callbackResponse) {

        mAPI.getAllNews(mCurrentToken, new Callback<NewsListResponse>() {
            @Override
            public void success(NewsListResponse newsListResponse, Response response) {
                callbackResponse.onSuccess(newsListResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                callbackResponse.onFailure(error);
            }
        });
    }

    @Override
    public void readNewNews(final String lastDate, final GenericCallback<NewsListResponse> callbackResponse) {
        final String object = "{\"createdAt\":{\"$gt\":\"" + lastDate + "\"}}";
        mAPI.getNewNews(News.NewsConstants.KEY_DATE, object, new Callback<NewsListResponse>() {

            @Override
            public void success(final NewsListResponse newsListResponse, final Response response) {
                callbackResponse.onSuccess(newsListResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                callbackResponse.onFailure(error);
            }
        });
    }

    @Override
    public void signUp(String email, String username, String password,String passwordConfirmation, final GenericCallback<SignUpResponse> userGenericCallback) {
        final User newUser = new User(email, username, password, passwordConfirmation);
        final UserRequest userRequest = new UserRequest(newUser);
        mAPI.createNewUser(userRequest, new Callback<SignUpResponse>() {
            @Override
            public void success(SignUpResponse signUpResponse, Response response) {
                Log.d(TAG_SIGN_IN, mContext.getString(R.string.log_succes));
                userGenericCallback.onSuccess(signUpResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG_SIGN_IN, mContext.getString(R.string.log_failure));
                userGenericCallback.onFailure(error);
            }
        });
    }

    @Override
    public void getChosenNews(final int id, final GenericCallback<NewsResponse> newsGenericCallback) {
        mAPI.getNews(mCurrentToken, id, new Callback<NewsResponse>() {
            @Override
            public void success(final NewsResponse newsResponse, final Response response) {
                newsGenericCallback.onSuccess(newsResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e("getChosenNews:error", error.getUrl() + " || " + error.getMessage());

            }
        });
    }

    @Override
    public void putVote(final int id,final boolean voteValue, final GenericCallback<VoteResponse> voteResponseGenericCallback) {

        final VoteRequest voteRequest = new VoteRequest(new Vote(voteValue));
        mAPI.putVote(mCurrentToken, id, voteRequest, new Callback<VoteResponse>() {
            @Override
            public void success(final VoteResponse voteResponse, final Response response) {
                voteResponseGenericCallback.onSuccess(voteResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                voteResponseGenericCallback.onFailure(error);
            }
        });
    }

    @Override
    public void postVote(final int id, final boolean voteValue,final GenericCallback<VoteResponse> voteResponseGenericCallback) {
        final VoteRequest voteRequest = new VoteRequest(new Vote(voteValue));
        mAPI.postVote(mCurrentToken, id, voteRequest, new Callback<VoteResponse>() {
            @Override
            public void success(final VoteResponse voteResponse, final Response response) {
                voteResponseGenericCallback.onSuccess(voteResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                voteResponseGenericCallback.onFailure(error);
            }
        });
    }

    @Override
    public void signOut(final GenericCallback<SignOutResponse> userGenericCallback) {

        mAPI.signOut(mCurrentToken, new Callback<SignOutResponse>() {
            @Override
            public void success(SignOutResponse signOutResponse, Response response) {
                userGenericCallback.onSuccess(signOutResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                userGenericCallback.onFailure(error);
            }
        });

    }

    @Override
    public void getCategories(final GenericCallback<CategoryListResponse> categoryListResponseGenericCallback) {
        mAPI.getCategories(mCurrentToken, new Callback<CategoryListResponse>() {
            @Override
            public void success(final CategoryListResponse categoryListResponse, final Response response) {
                categoryListResponseGenericCallback.onSuccess(categoryListResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                categoryListResponseGenericCallback.onFailure(error);
            }
        });
    }

    @Override
    public void getUserMe(final GenericCallback<UserResponse> userMeResponseGenericCallback) {
        mAPI.getUserMe(mCurrentToken, new Callback<UserResponse>() {
            @Override
            public void success(final UserResponse userResponse, final Response response) {
                userMeResponseGenericCallback.onSuccess(userResponse);
                mTmpUser.setEmail(userResponse.getUser().getEmail());
                mTmpUser.setListOfCategory(userResponse.getUser().getListOfCategory());
            }

            @Override
            public void failure(final RetrofitError error) {
                userMeResponseGenericCallback.onFailure(error);
            }
        });
    }

    @Override
    public void getUserNews(final GenericCallback<NewsListResponse> userNewsResponseCallback) {
        mAPI.getUserNews(mCurrentToken, new Callback<NewsListResponse>() {
            @Override
            public void success(final NewsListResponse newsListResponse, final Response response) {
                userNewsResponseCallback.onSuccess(newsListResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                userNewsResponseCallback.onFailure(error);
            }
        });

    }

    @Override
    public void getTopNews(final GenericCallback<NewsListResponse> topNewsResponseCallback) {

        mAPI.getTopNews(mCurrentToken, new Callback<NewsListResponse>() {
            @Override
            public void success(final NewsListResponse newsListResponse, final Response response) {
                topNewsResponseCallback.onSuccess(newsListResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                topNewsResponseCallback.onFailure(error);
            }
        });

    }

    @Override
    public void setUser(final List<Integer> categoriesIds, final GenericCallback<UserResponse> setUserResponseCallback) {

        mTmpUser.setCategoryIds(categoriesIds);
        final UserRequest userRequest = new UserRequest(mTmpUser);//(username, password));
        mAPI.setUser(mCurrentToken, userRequest, new Callback<UserResponse>() {
            @Override
            public void success(final UserResponse userResponse, final Response response) {
                setUserResponseCallback.onSuccess(userResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                setUserResponseCallback.onFailure(error);
            }
        });

    }

    @Override
    public void postFollowedPeople(final FollowedPerson followedPerson, final GenericCallback<SuccessFailResponse> userGenericCallback) {
        User user = new User(followedPerson);
        mAPI.postFollowedPeople(mCurrentToken, new UserRequest(user), new Callback<SuccessFailResponse>() {
            @Override
            public void success(final SuccessFailResponse successFailResponse, final Response response) {
                userGenericCallback.onSuccess(successFailResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                userGenericCallback.onFailure(error);
            }
        });
    }

    @Override
    public void getFollowedPeople(final GenericCallback<PeopleListResponse> userGenericCallback) {
        mAPI.getFollowedPeople(mCurrentToken, new Callback<PeopleListResponse>() {
            @Override
            public void success(final PeopleListResponse peopleListResponse, final Response response) {
                    userGenericCallback.onSuccess(peopleListResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                userGenericCallback.onFailure(error);
            }
        });
    }

    @Override
    public void deleteFollowedPeople(final int id, final GenericCallback<SuccessFailResponse> userGenericCallback) {
        mAPI.deleteFollowedPeople(mCurrentToken, id, new Callback<SuccessFailResponse>() {
            @Override
            public void success(final SuccessFailResponse successFailResponse, final Response response) {
                userGenericCallback.onSuccess(successFailResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                userGenericCallback.onFailure(error);
            }
        });
    }

    @Override
    public void getFollowedPeopleNews(final GenericCallback<NewsListResponse> userNewsResponseCallback) {
        mAPI.getFollowedPeopleNews(mCurrentToken, new Callback<NewsListResponse>() {
            @Override
            public void success(final NewsListResponse newsListResponse, final Response response) {
                userNewsResponseCallback.onSuccess(newsListResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                userNewsResponseCallback.onFailure(error);
            }
        });
    }

    @Override
    public void putFollowedPeople(final FollowedPerson followedPerson, final GenericCallback<SuccessFailResponse> userGenericCallback) {
        User user = new User(followedPerson);
        // Id biorę z Person teraz
        mAPI.putFollowedPeople(mCurrentToken, followedPerson.getId(), new UserRequest(user), new Callback<SuccessFailResponse>() {
            @Override
            public void success(final SuccessFailResponse successFailResponse, final Response response) {
                userGenericCallback.onSuccess(successFailResponse);
            }

            @Override
            public void failure(final RetrofitError error) {
                userGenericCallback.onFailure(error);
            }
        });
    }


    @Override
    public void logIn(final String username, final String password, final GenericCallback<UserResponse> userGenericCallback) {

        mTmpUser = new User();
        final UserRequest userRequest = new UserRequest(new User(username, password));//adrianx, adrian111
        mAPI.logIn(userRequest, new Callback<UserResponse>() {
            @Override
            public void success(UserResponse userResponse, Response response) {
                Log.d(TAG_LOG_IN, mContext.getString(R.string.log_succes));
                userGenericCallback.onSuccess(userResponse);
                setHeader(userResponse.getUser().getSession().getToken());
                mTmpUser.setCategoriesSet(userResponse.getUser().isCategoriesSet());

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG_LOG_IN, error.getMessage());
                userGenericCallback.onFailure(error);

            }
        });
    }

    private interface ParseConfig {
        String RETROFIT_LOG = "RETROFIT_PARSE_LOG: ";
        String PARSE_ENDPOINT = "http://news-world.iiar.pwr.edu.pl/api/v1";//"http://news-world.iiar.pwr.edu.pl";
        String TOKEN = "Token ";
    }

}
