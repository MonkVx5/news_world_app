package com.example.adrianmatuszewski.news_world.Model.response;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public class CategoryListResponse {

    @SerializedName(CategoriesConstant.CATEGORIES)
    private List<Category> mCategories;

    public List<Category> getCategories() {
        return mCategories;
    }

    public interface CategoriesConstant {
        String CATEGORIES = "categories";
    }
}
