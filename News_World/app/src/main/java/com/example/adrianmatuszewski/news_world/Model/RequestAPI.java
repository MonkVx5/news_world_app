package com.example.adrianmatuszewski.news_world.Model;


import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.FollowedPerson;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;
import com.example.adrianmatuszewski.news_world.Model.request.UserRequest;
import com.example.adrianmatuszewski.news_world.Model.request.VoteRequest;
import com.example.adrianmatuszewski.news_world.Model.response.CategoryListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.NewsListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.NewsResponse;
import com.example.adrianmatuszewski.news_world.Model.response.PeopleListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SignOutResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SignUpResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SuccessFailResponse;
import com.example.adrianmatuszewski.news_world.Model.response.UserResponse;
import com.example.adrianmatuszewski.news_world.Model.response.VoteResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;


/**
 * @author: Adrian Matuszewski
 */
public interface RequestAPI {

    @GET("/news")
    void getAllNews(@Header("Authorization")String token, Callback<NewsListResponse> callback);

    @POST("/users/sign_in")
    void logIn(@Body UserRequest userRequest, Callback<UserResponse> userCallback);

    @GET("/1/classes/News")
    void getNewNews(@Query("order") String createdAt, @Query("where") String lastCreatedAt, Callback<NewsListResponse> callback);

    @POST("/users/sign_up")
    void createNewUser(@Body UserRequest userRequest, Callback<SignUpResponse> newUserCallback);

    @GET("/news/{id}")
    void getNews(@Header("Authorization")String token,@Path(News.NewsConstants.KEY_ID) int objectId, Callback<NewsResponse> callback);

    @DELETE("/users/sign_out")
    void signOut(@Header("Authorization")String token, Callback<SignOutResponse> callback);

    @GET("/categories")
    void getCategories(@Header("Authorization")String token, Callback<CategoryListResponse> callback);

    @GET("/users/me")
    void getUserMe(@Header("Authorization")String token, Callback<UserResponse> callback);

    @GET("/users/me/news")
    void getUserNews(@Header("Authorization")String token, Callback<NewsListResponse> callback);

    @GET("/users/me/news/top")
    void getTopNews(@Header("Authorization")String token, Callback<NewsListResponse> callback);

    @PUT("/users/me")
    void setUser(@Header("Authorization")String token,@Body UserRequest userRequest, Callback<UserResponse> callback);

    @PUT("/news/{id}/votes")
    void putVote(@Header("Authorization")String token, @Path(News.NewsConstants.KEY_ID) int objectId,@Body VoteRequest voteRequest, Callback<VoteResponse> callback);

    @POST("/news/{id}/votes")
    void postVote(@Header("Authorization")String token, @Path(News.NewsConstants.KEY_ID) int objectId,@Body VoteRequest voteRequest, Callback<VoteResponse> callback);

    ///Following:
    @POST("/users/me/followed_people")
    void postFollowedPeople(@Header("Authorization")String token,@Body UserRequest userRequest, Callback<SuccessFailResponse> callback);

    @GET("/users/me/followed_people")
    void getFollowedPeople(@Header("Authorization")String token, Callback<PeopleListResponse> callback);

    @DELETE("/users/me/followed_people/{id}")
    void deleteFollowedPeople(@Header("Authorization")String token,@Path(FollowedPerson.FollowedPersonConstant.KEY_ID) int objectId, Callback<SuccessFailResponse> callback);

    @GET("/users/me/news/followed_people")
    void getFollowedPeopleNews(@Header("Authorization")String token, Callback<NewsListResponse> callback);

    @PUT("/users/me/followed_people/{id}")
    void putFollowedPeople(@Header("Authorization")String token,@Path(FollowedPerson.FollowedPersonConstant.KEY_ID) int objectId, @Body UserRequest userRequest, Callback<SuccessFailResponse> callback);


}
