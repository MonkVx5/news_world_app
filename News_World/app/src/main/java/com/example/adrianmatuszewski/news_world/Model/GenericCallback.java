package com.example.adrianmatuszewski.news_world.Model;

/**
 * @author: Adrian Matuszewski
 */
public interface GenericCallback<T> {

    void onSuccess(T data);

    void onFailure(Exception error);
}
