package com.example.adrianmatuszewski.news_world.Presenter;



import com.example.adrianmatuszewski.news_world.Model.DataSource;
import com.example.adrianmatuszewski.news_world.Model.GenericCallback;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.example.adrianmatuszewski.news_world.Model.response.CategoryListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.NewsListResponse;
import com.example.adrianmatuszewski.news_world.Model.RetrofitDataSource;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;
import com.example.adrianmatuszewski.news_world.Model.response.SignOutResponse;
import com.example.adrianmatuszewski.news_world.Model.response.UserResponse;
import com.example.adrianmatuszewski.news_world.View.ListOfNewsView;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public class ListOfNewsPresenter<T extends ListOfNewsView> extends Presenter<T>  {

    private static final int TIMER_DELAY = 800;

    private DataSource mDataSource;

    private List<News> mListOfNews;
    private List<News> mListOfAllNews;
    private List<News> mListOfUserNews;
    private List<News> mListOfTopNews;
    private List<News> mListFollowedPersonNews;

    private ArrayList<Category> mListOfCategories;

    private int mCurrentView = 3; //3 -all, 2 - my, 1- top 4- followed //TODO: Hmmm, I should to use here maybe enum type.

    public ListOfNewsPresenter(final Context context) {
        mDataSource = new RetrofitDataSource(context);
        mListOfNews = new ArrayList();
        getAllNews();
        prepareAllCategories();
    }

    public void  prepareAllCategories() {
        mDataSource.getCategories(new GenericCallback<CategoryListResponse>() {
            @Override
            public void onSuccess(final CategoryListResponse data) {
                mListOfCategories = (ArrayList<Category>) data.getCategories();
                mView.prepareCategoryTableLayout(mListOfCategories);
            }

            @Override
            public void onFailure(final Exception error) {
                error.getMessage();
            }
        });
    }

    public void prepareUserCategories() {
        mDataSource.getUserMe(new GenericCallback<UserResponse>() {
            @Override
            public void onSuccess(final UserResponse data) {
                ArrayList<Category> userCategories = (ArrayList<Category>) data.getUser().getListOfCategory();
                mView.prepareCategoryTableLayout(userCategories);
            }

            @Override
            public void onFailure(final Exception error) {
                error.getMessage();
            }
        });
    }



    public void signOut() {
        mDataSource.signOut(new GenericCallback<SignOutResponse>() {
            @Override
            public void onSuccess(SignOutResponse data) {
                mView.signOut();
            }

            @Override
            public void onFailure(Exception error) {
                Toast.makeText(mView.getContext(), "problem", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void getAllNews() {
        mDataSource.getAllNews(new GenericCallback<NewsListResponse>() {
            @Override
            public void onSuccess(NewsListResponse data) {
                mListOfAllNews = data.getListOfNews();
                mListOfNews.addAll(mListOfAllNews);
                mView.setList(sortByDate(mListOfNews));
            }

            @Override
            public void onFailure(Exception error) {

                Toast.makeText(mView.getContext(), "problem", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getTopNews() {
        mDataSource.getTopNews(new GenericCallback<NewsListResponse>() {
            @Override
            public void onSuccess(NewsListResponse data) {
                mListOfTopNews = data.getListOfNews();
                mListOfNews.addAll(mListOfTopNews);
                mView.changeCurrentList(sortByDate(mListOfNews));
            }

            @Override
            public void onFailure(Exception error) {

                Toast.makeText(mView.getContext(), "problem", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getFollowedPersonNews() {
        mDataSource.getFollowedPeopleNews(new GenericCallback<NewsListResponse>() {
            @Override
            public void onSuccess(NewsListResponse data) {
                mListFollowedPersonNews = data.getListOfNews();
                mListOfNews.addAll(mListFollowedPersonNews);
                mView.changeCurrentList(sortByDate(mListFollowedPersonNews));
            }

            @Override
            public void onFailure(Exception error) {

                Toast.makeText(mView.getContext(), "problem", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void showFollowedPeopleNews() {
        mView.prepareCategoryTableLayout(mListOfCategories);
        mListOfNews.clear();
        getFollowedPersonNews();
        mCurrentView = 4;
    }

    public void showAllNews() {
        mView.prepareCategoryTableLayout(mListOfCategories);
        mListOfNews.clear();
        mListOfNews.addAll(mListOfAllNews);
        mView.changeCurrentList(sortByDate(mListOfNews));
        mCurrentView = 3;
    }

    public void showMyNews() {
        prepareUserCategories();
        mListOfNews.clear();
        getUserNews();
        mCurrentView = 2;
    }

    public void showTopNews() {
        prepareUserCategories();
        mListOfNews.clear();
        getTopNews();
        mCurrentView = 1;
    }

    public void getUserNews() {
        mDataSource.getUserNews(new GenericCallback<NewsListResponse>() {
            @Override
            public void onSuccess(NewsListResponse data) {
                mListOfUserNews = data.getListOfNews();
                mListOfNews.addAll(mListOfUserNews);
                mView.changeCurrentList(sortByDate(mListOfNews));
            }

            @Override
            public void onFailure(Exception error) {
                Toast.makeText(mView.getContext(), "problem user news", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public List<News> filter(final String query) {
        String tittle = "";
        String content = "";
        String source = "";
        String category = "";
        final List<News> filteredListOfNews = new ArrayList<>();
        for (News news : mListOfNews) {
            if (news.getTittle() != null) {
                tittle = news.getTittle().toLowerCase();
            }
            if (news.getTittle() != null) {
                content = news.getContent().toLowerCase();
            }
            if (news.getTittle() != null) {
                source = news.getSource().toLowerCase();
            }
            if (news.getCategory() != null) {
                category = news.getCategory().getName().toLowerCase();
            }
            if (tittle.contains(query) || content.contains(query) || source.contains(query) || category.contains(query) ) {
                filteredListOfNews.add(news);
            }
        }
        return filteredListOfNews;
    }

    public List<News> filterByCategory(final String query) {
        String category = "";
        final List<News> filteredListOfNews = new ArrayList<>();
        if (mCurrentView == 3 && mListOfAllNews != null) {
            for (News news : mListOfAllNews ) {
                if (news.getCategory() != null) {
                    category = news.getCategory().getName().toLowerCase();
                }
                if (category.contains(query)) {
                    filteredListOfNews.add(news);
                }
            }
        } else if (mCurrentView == 2 && mListOfUserNews != null) {
            for (News news : mListOfUserNews) {
                if (news.getCategory() != null) {
                    category = news.getCategory().getName().toLowerCase();
                }
                if (category.contains(query)) {
                    filteredListOfNews.add(news);
                }
            }
        } else if (mCurrentView == 1 && mListOfTopNews != null) {
            for (News news : mListOfTopNews) {
                if (news.getCategory() != null) {
                    category = news.getCategory().getName().toLowerCase();
                }
                if (category.contains(query)) {
                    filteredListOfNews.add(news);
                }
            }
        }else if (mCurrentView == 1 && mListFollowedPersonNews != null) {
            for (News news : mListFollowedPersonNews) {
                if (news.getCategory() != null) {
                    category = news.getCategory().getName().toLowerCase();
                }
                if (category.contains(query)) {
                    filteredListOfNews.add(news);
                }
            }
        }
        mListOfNews = filteredListOfNews;
        return filteredListOfNews;
    }


    private List<News> sortByDate(final List<News> list) {
        final List<News> news = list;
        Collections.sort(news, new Comparator<News>() {
            @Override
            public int compare(final News o1, final News o2) {
                return o1.getSource().compareTo(o2.getPublishDate());
            }
        });
        return news;
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }
    @Override
    public void finish() {

    }


}
