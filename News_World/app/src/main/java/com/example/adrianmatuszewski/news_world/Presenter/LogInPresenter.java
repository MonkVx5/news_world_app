package com.example.adrianmatuszewski.news_world.Presenter;



import com.example.adrianmatuszewski.news_world.Model.DataSource;
import com.example.adrianmatuszewski.news_world.Model.GenericCallback;
import com.example.adrianmatuszewski.news_world.Model.RetrofitDataSource;
import com.example.adrianmatuszewski.news_world.Model.response.UserResponse;
import com.example.adrianmatuszewski.news_world.R;
import com.example.adrianmatuszewski.news_world.View.LogInView;

import android.content.Context;
import android.widget.Toast;

/**
 * @author: Adrian Matuszewski
 */
public class LogInPresenter  <T extends LogInView> extends Presenter<T>  {

    private DataSource mDataSource;
    private Context mContext;

    public LogInPresenter(final Context context) {
        mDataSource = new RetrofitDataSource(context);
        mContext = context;
    }

    public void tryLogIn(String username, String password) {
        mView.hideKeyboard();
        mDataSource.logIn(username, password, new GenericCallback<UserResponse>() {
            @Override
            public void onSuccess(UserResponse data) {
                if (data.getUser().isCategoriesSet()) {
                    mView.goToListOfNewsActivity();
                } else {
                    mView.goToListOfNewsActivity();
                    mView.openSettings();
                }
                mView.finish();
            }

            @Override
            public void onFailure(Exception error) {
                Toast.makeText(mContext, mContext.getString(R.string.log_failure), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void goToSignUpActivity() {
        mView.goToSignActivity();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void finish() {

    }

}
