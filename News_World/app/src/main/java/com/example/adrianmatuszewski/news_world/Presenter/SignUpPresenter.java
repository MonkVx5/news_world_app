package com.example.adrianmatuszewski.news_world.Presenter;



import com.example.adrianmatuszewski.news_world.Model.DataSource;
import com.example.adrianmatuszewski.news_world.Model.GenericCallback;
import com.example.adrianmatuszewski.news_world.Model.RetrofitDataSource;
import com.example.adrianmatuszewski.news_world.Model.response.SignUpResponse;
import com.example.adrianmatuszewski.news_world.View.SignUpView;

import android.content.Context;
import android.widget.Toast;

/**
 * @author: Adrian Matuszewski
 */
public class SignUpPresenter<T extends SignUpView> extends Presenter<T>  {

    private DataSource mDataSource;
    private Context mContext;

    public SignUpPresenter(final Context context) {
        mDataSource = new RetrofitDataSource(context);
        mContext = context;
    }

    public void tryCreateAccount(String email, String username, String password, String passwordConfirmation) {
        mView.hideKeyboard();
        mDataSource.signUp(email, username, password, passwordConfirmation, new GenericCallback<SignUpResponse>() {
            @Override
            public void onSuccess(SignUpResponse data) {
                Toast.makeText(mContext, "New account created", Toast.LENGTH_SHORT).show();
                mView.goToLogInActivity();
            }

            @Override
            public void onFailure(Exception error) {
                Toast.makeText(mContext, "Problem with creating new account", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void finish() {

    }

}
