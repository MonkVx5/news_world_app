package com.example.adrianmatuszewski.news_world.Model.dataBaseModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author: Adrian Matuszewski
 */
public class Category implements Serializable {

    @SerializedName(CategoryConstant.ID)
    private final int mId;
    @SerializedName(CategoryConstant.NAME)
    private final String mName;

    private boolean mIsSetByUser;

    public Category(final int id, final String name) {
        mId = id;
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public boolean isSetByUser() {
        return mIsSetByUser;
    }

    public void setIsSetByUser(boolean isSetByUser) {
        mIsSetByUser = isSetByUser;
    }

    public interface CategoryConstant {
        String ID = "id";
        String NAME = "name";
    }
}
