package com.example.adrianmatuszewski.news_world.Presenter;

import com.example.adrianmatuszewski.news_world.Model.DataSource;
import com.example.adrianmatuszewski.news_world.Model.GenericCallback;
import com.example.adrianmatuszewski.news_world.Model.RetrofitDataSource;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.Category;
import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.FollowedPerson;
import com.example.adrianmatuszewski.news_world.Model.response.CategoryListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.PeopleListResponse;
import com.example.adrianmatuszewski.news_world.Model.response.SuccessFailResponse;
import com.example.adrianmatuszewski.news_world.View.AddFollowedPeopleView;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

/**
 * @author: Adrian Matuszewski
 */
public class AddFollowedPeoplePresenter <T extends AddFollowedPeopleView> extends Presenter<T>  {

    private DataSource mDataSource;
    private ArrayList<FollowedPerson> mFollowedPersonList;

    public AddFollowedPeoplePresenter(final Context context) {
        mDataSource = new RetrofitDataSource(context);

    }

    public void prepareListOfFollowedPeople() {
        mDataSource.getFollowedPeople(new GenericCallback<PeopleListResponse>() {
            @Override
            public void onSuccess(final PeopleListResponse data) {
                mFollowedPersonList = (ArrayList<FollowedPerson>) data.getListOfPeople();
                mView.setList(mFollowedPersonList);
            }

            @Override
            public void onFailure(final Exception error) {
                Log.d("AddFollowedPeoplePres","getFollowedPeople " + error );
            }
        });
    }

    public void addFollowedPerson(String name, String surname, String profession) {
        FollowedPerson followedPerson = new FollowedPerson(name, surname, profession);
        mDataSource.postFollowedPeople(followedPerson, new GenericCallback<SuccessFailResponse>() {
            @Override
            public void onSuccess(final SuccessFailResponse data) {
                Log.d("AddFollowedPeoplePres", "person added ");
            }

            @Override
            public void onFailure(final Exception error) {
                Log.d("AddFollowedPeoplePres", "addFollowedPerson " + error);
            }
        });
    }

    public void deletePerson(final int id) {
        mDataSource.deleteFollowedPeople(id, new GenericCallback<SuccessFailResponse>() {
            @Override
            public void onSuccess(final SuccessFailResponse data) {
                Log.d("AddFollowedPeoplePres", "person deleted ");

            }

            @Override
            public void onFailure(final Exception error) {
                Log.d("AddFollowedPeoplePres", "deletePerson " + error);
            }
        });
    }
    
    @Override
    void resume() {

    }

    @Override
    void pause() {

    }

    @Override
    void destroy() {

    }

    @Override
    void finish() {

    }
}
