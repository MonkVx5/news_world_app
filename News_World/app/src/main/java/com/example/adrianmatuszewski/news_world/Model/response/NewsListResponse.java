package com.example.adrianmatuszewski.news_world.Model.response;


import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.News;

import java.util.List;
/**
 * @author: Adrian Matuszewski
 */
public class NewsListResponse {

    private List<News> news;

    public List<News> getListOfNews() {
            return news;
    }

    public void setListOfNews(List<News> news) {
        this.news = news;
    }

}