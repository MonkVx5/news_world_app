package com.example.adrianmatuszewski.news_world.Model.response;

import com.example.adrianmatuszewski.news_world.Model.dataBaseModel.User;

/**
 * @author: Adrian Matuszewski
 */
public class UserResponse {

    private User user;

    public User getUser() {
        return user;
    }

}
