package com.example.adrianmatuszewski.news_world.View;

/**
 * @author: Adrian Matuszewski
 */
public interface SignUpView extends BaseView {

    void goToLogInActivity();

    void hideKeyboard();
    
}
